@extends('backend.layouts.index')
<!-- Datatables -->
<link href="{{ url('backend/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('backend/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('backend/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('backend/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('backend/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@section('content')
<div class="col-md-12 ">
    <div class="x_panel">
        <a class="float-right btn btn-default" href="{{route('user.create')}}">Tambah</a>
        @csrf
        <div class="x_title">
            <table id="datatable-responsive" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Email</th>
                        <th>Nama</th>
                        <th>Usia</th>
                        <th>Jenis Kelamin</th>
                        <th>Option</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user as $key => $item)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->usia}}</td>
                        <td>{{$item->jenis_kelamin_label}}</td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-primary" href="{{route('user.edit', $item->id)}}">Edit</a>
                                <form action="{{route('user.destroy', $item->id)}}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
<!-- Datatables -->
<script src="{{ url('backend/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ url('backend/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ url('backend/vendors/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ url('backend/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ url('backend/vendors/pdfmake/build/vfs_fonts.js') }}"></script>
@endsection
