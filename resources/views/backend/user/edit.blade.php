@extends('backend.layouts.index')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card-body">
        <form method="POST" action="{{route('user.update', $user->id)}}" class="form-horizontal form-label-left">
            @csrf
            @method('PUT')
            
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email<span
                        class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="email" id="email" name="email"
                        class="form-control col-md-7 col-xs-12" value="{{$user->email}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="name" class="form-control col-md-7 col-xs-12" value="{{$user->name}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Usia <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="usia" class="form-control col-md-7 col-xs-12" value="{{$user->usia}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div id="jenis_kelamin" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default {{$user->jenis_kelamin == 0 ? 'active' : ''}}" data-toggle-class="btn-primary"
                            data-toggle-passive-class="btn-default">
                            <input type="radio" name="jenis_kelamin" value="0" {{$user->jenis_kelamin == 0 ? 'checked' : ''}}> &nbsp; Laki-laki &nbsp;
                        </label>
                        <label class="btn btn-default {{$user->jenis_kelamin == 1 ? 'active' : ''}}" data-toggle-class="btn-primary"
                            data-toggle-passive-class="btn-default">
                            <input type="radio" name="jenis_kelamin" value="1" {{$user->jenis_kelamin == 1 ? 'checked' : ''}}> Perempuan
                        </label>                     
                    </div>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="password" class="form-control col-md-7 col-xs-12" type="password" name="password">
                </div>
            </div>
            
            
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <a class="btn btn-primary" href="{{route('user.index')}}">Cancel</a>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
</div>
@endsection
