<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li class="nav-item">
                <a class="nav-link " href="{{url('/home')}}"> <i class="fa fa-dashboard"></i>Dashboard</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="{{route('user.index')}}"> <i class="fa fa-user"></i> User </span></a>
            </li>
        </ul>
    </div>
</div>