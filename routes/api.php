<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'Api'], function (){
Route::post('/user', 'UserController@store' );
Route::get('/users', 'UserController@index' );
Route::get('/user/{id}', 'UserController@indexById' );
Route::get('/users/{jenis_kelamin}', 'UserController@indexGender' );



//    Route::put('/user/{id}', function ($id) {
//     return response()->json(
//         [
//             "massage"=> "PUT Method Succes" .$id
//         ]
//     );
//    });

//    Route::delete('/user/{id}', function ($id) {
//     return response()->json(
//         [
//             "massage"=> "DELETE Method Succes" .$id
//         ]
//     );
//    });

});