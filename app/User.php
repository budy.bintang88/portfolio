<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','name','usia','jenis_kelamin','password',
    ];

   
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'role', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getJenisKelaminLabelAttribute()
    {
        $jenis_kelamin = '-';

        if ($this->jenis_kelamin == 0){
            $jenis_kelamin = "Laki_laki";
        }elseif ($this->jenis_kelamin = 1){
            $jenis_kelamin = "Perempuan";
        }
        
        return $jenis_kelamin;
    }
}
