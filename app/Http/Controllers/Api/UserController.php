<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $user = User::all(); 
        return response()->json(
            [
                "status" => 200,
                "message" => "Berhasil",
                "data" => $user
            ]
        );
    }
    public function indexById($id)
    {
        
        $user = User::where('id', $id)->get(); 

        return response()->json(
            [
                "status" => 200,
                "message" => "Berhasil",
                "data" => $user
            ]
        );
    }

    public function indexGender($id)
    {
        
        $user = User::where('jenis_kelamin',$id)->orderBy('usia','desc')->get(); 
        return response()->json(
            [
                "status" => 200,
                "message" => "Berhasil",
                "data" => $user
            ]
        );
    }

    
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()//GET
    {
        return view('backend.user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//tambah data
    {
        
        request()->validate([
        'email'=>'required',
        'name'=>'required',
        'usia'=>'required',
        'jenis_kelamin'=>'required',
        'password'=>'required',
        ]);
       
        $users = New User;
       
        $users->name = request()->name;
        $users->email = request()->email;
        $users->usia = request()->usia;
        $users->jenis_kelamin = request()->jenis_kelamin;
        $users->password = Hash::make(request()->password);
        
        $users->save();

         return response()->json(
            [
                "status" => 200,
                "message" => "Data berhasil dibuat",
                "data" => $users
            ]
            );

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        //return $user;
        return view('backend.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // memvalidasi input data yang harus terisi 
        request()->validate([
            'email'=>'required',
            'name'=>'required',
            'usia'=>'required',
            'jenis_kelamin'=>'required',
            'password',
            ]);
            //variable update
            $users = User::findOrFail($id);
            //keterangan permintaan inputan
            $users->name = request()->name;
            $users->email = request()->email;
            $users->usia = request()->usia;
            $users->jenis_kelamin = request()->jenis_kelamin;
            if (request()->password) {
                $users->password = Hash::make(request()->password);
            }else{
                //
            }
            
            //menyimpan data
            $users->save();
            return response()->json(
                [
                    "status" => 200,
                    "message" => "Data berhasil diubah",
                    "data" => $users
                ]
                );
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $user->delete();
        return response()->json(
            [
                "status" => 200,
                "message" => "Data berhasil dihapus",
                "data" => $users
            ]
            );
    }
}
