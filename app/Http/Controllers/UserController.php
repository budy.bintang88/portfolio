<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // variable untuk mengambail data agar terlihat di view
        $user = User::all();
        
        return view('backend.user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()//GET
    {
        return view('backend.user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validasi inputan
        request()->validate([
        'email'=>'required',
        'name'=>'required',
        'usia'=>'required',
        'jenis_kelamin'=>'required',
        'password'=>'required',
        ]);
        //variable tambah
        $users = New User;
        //keterangan permintaan inputan
        $users->name = request()->name;
        $users->email = request()->email;
        $users->usia = request()->usia;
        $users->jenis_kelamin = request()->jenis_kelamin;
        $users->password = Hash::make(request()->password);
        //menyimpan data
        $users->save();

        if ($users->save()){
            return redirect('home/user');
        }else{
            return redirect()->back()->withErrors();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        //return $user;
        return view('backend.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // memvalidasi input data yang harus terisi 
        request()->validate([
            'email'=>'required',
            'name'=>'required',
            'usia'=>'required',
            'jenis_kelamin'=>'required',
            'password',
            ]);
            //variable update
            $users = User::findOrFail($id);
            //keterangan permintaan inputan
            $users->name = request()->name;
            $users->email = request()->email;
            $users->usia = request()->usia;
            $users->jenis_kelamin = request()->jenis_kelamin;
            if (request()->password) {
                $users->password = Hash::make(request()->password);
            }else{
                //
            }
            
            //menyimpan data
            $users->save();
    
            if ($users->save()){
                return redirect('home/user');
            }else{
                return redirect()->back()->withErrors();
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $user->delete();
        // return $user;
        return redirect()->back();
    }
}